#!/usr/bin/env sh

# Test that we can connect to the SSL server with a crt signed by the CA in ADDITIONAL_CA_CERT_BUNDLE
# If the Self-Signed Cert is installed in the trust store(/etc/ssl/certs/[ca-bundle.crt / ca-certificates.crt])
# then wget request passes through without error


URL="https://ssl-test/"

# Checks if the given CLI command is
# installed in the system
cmd_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Runs the given CLI command. If the result
# of the command is non-zero, prints error message and
# exits the session
run_cmd() {
  $@
  if [ $? -ne 0 ]; then
    echo -e "\nERROR: CA bundle cert validation failed"
    exit 1
  fi
  echo -e "\nSUCCESS: CA bundle cert validation passed"
}

# Installs `wget` library based on the OS variant.
# Currently, the implementation is added only for
# for the debian-based OS since other OS variants
# we use comes wget/curl pre-installed.
install_wget() {
  if [ -f "/etc/debian_version" ]; then
    apt-get update && apt-get install wget -y
    if [ $? -ne 0 ]; then
      echo -e "\nfailed to install `wget`, exiting.."
      exit 1
    fi
  else
    echo "\n`wget` installation for this OS variant is not implemented. Variant info: \n $(cat /etc/os-release)"
    echo "\nAdd it here: https://gitlab.com/gitlab-org/security-products/tests/custom-ca/-/blob/master/check-cert.sh#L38"
    exit 1
  fi
}


if cmd_exists wget; then
  echo -e "requesting '$URL' via wget..\n"
  run_cmd wget --spider $URL
elif cmd_exists curl; then
  echo -e "requesting '$URL' via curl..\n"
  run_cmd curl $URL
else
  echo -e "wget/curl not found, attempting to install 'wget'..\n"
  install_wget
  run_cmd wget --spider $URL
fi


