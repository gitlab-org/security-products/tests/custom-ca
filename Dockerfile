ARG BASE_IMAGE

FROM $BASE_IMAGE

COPY test.sh /
COPY check-cert.sh /

CMD /test.sh
